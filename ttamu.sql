-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 20 Bulan Mei 2023 pada 04.40
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbbukutamu`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ttamu`
--

CREATE TABLE `ttamu` (
  `id` int(10) NOT NULL,
  `tanggal` date NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `tujuan` varchar(1000) NOT NULL,
  `nope` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `ttamu`
--

INSERT INTO `ttamu` (`id`, `tanggal`, `nama`, `alamat`, `tujuan`, `nope`) VALUES
(1, '2023-03-24', 'nurbaiti', 'penggajawa', 'halloooo', '7123456'),
(2, '2023-03-24', 'yusra', 'keo', 'hay', '823142'),
(3, '2023-03-24', 'yusra', 'keo', 'hay', '823142'),
(4, '2023-03-24', 'nurbaiti', 'penggajawa', 'hallooo..ingin bertemu dengan ustadz Wahab!!', '823142'),
(5, '2023-05-05', 'nurbaiti H.R', 'penggjawa', 'hay....', '2147483647'),
(6, '2023-05-05', 'Ulum Tahirah', 'anaraja', 'hfhdhfh', '2147483647'),
(7, '2023-05-05', 'Indri Syahrini', 'anaraja', '5464', '2147483647'),
(8, '2023-05-05', 'Ulum Tahirah', 'anaraja', 'r3r34r', '2147483647'),
(20, '2023-05-15', 'Sri', 'Jakarta', '', '082165785776'),
(21, '2023-05-15', 'Nandini', 'Korea', '', '082143564234'),
(22, '2023-05-15', 'Sri', 'Kupang', '', '082165785776'),
(23, '2023-05-15', 'Nandini', 'fgdfg', '', '082165785776'),
(24, '2023-05-15', 'Nandini', 'Kupang', '', '0821657857761'),
(25, '2023-05-15', 'Sri', 'Kupang', 'Rapat', '0821657857761'),
(26, '2023-05-15', 'sri', 'kupang', 'rapat', '0821657857761'),
(27, '2023-05-16', 'Almalia Syafira', 'Nangapenda', 'Cari Muka', '0857768675211'),
(28, '2023-05-17', 'Nandini', 'Kupang', 'rapat', '082165785776'),
(29, '2023-05-17', 'Nandini', 'Kupang', 'Rapat', '0821657857761'),
(30, '2023-05-17', 'nurbaiti', 'penggajwa', 'kunjungan', '087234563421'),
(31, '2023-05-17', 'nurbaiti', 'Kupang', 'Rapat', '0821657857761'),
(32, '2023-05-17', 'nurbaiti', 'Kupang', 'Rapat', '0821657857761'),
(33, '2023-05-17', 'nurbaiti ', 'Kupang', 'Rapat', '08213424356'),
(34, '2023-05-17', 'Nurbaiti ', 'Kupang ', 'Rapat', '0821657857761'),
(35, '2023-05-19', 'Nurbaiti ', 'Ende', 'Cari Muka', '082165785776'),
(36, '2023-05-19', 'Nandini', 'Jakarta', 'kunjungan', '0821657857761'),
(37, '2023-05-19', 'Irfani Tikase', 'Nangapanda', 'Kunjungan Multimedia ', '081236225119');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `ttamu`
--
ALTER TABLE `ttamu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `ttamu`
--
ALTER TABLE `ttamu`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
